/**
 * Copyright 2016  Kenneth Vermette <vermette@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) version 3, or any
 * later version accepted by the membership of KDE e.V. (or its
 * successor approved by the membership of KDE e.V.), which shall
 * act as a proxy defined in Section 6 of version 3 of the license.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

	function KWidgetContentPreload (data) {
		$.head = {};
		$.head.content = data;
	}

	(function( $ )
	{
		$(document).ready(function () {
			var primaryTabs = ['community', 'products', 'support', 'develop', 'donate'];

			$('header').append('<style id="KGlobalHeaderHeightFix"></style>');
			$('html').addClass('dynamicHeightFix');

			$('#KGlobalStaticNavigation li').has('section').addClass('hasMenu');


		});

		$(window).load(function () {
			if ($('#KSiteNavigation').hasClass('overlay')) {
				$('#KGlobalHeader + *.overlay, #KGlobalHeader + * .overlay').css('padding-top', $('#KSiteNavigation').outerHeight() + 'px');
			}
			/*
			else {
				$('#KGlobalHeaderHeightFix').text('.dynamicHeightFix { padding-top: '+$('#KGlobalHeader').height()+'px !important;}');
			}*/

			$(window).scroll(function(e) {
				$('#KGlobalHeader').toggleClass('sticky', $(window).scrollTop() > $('#KGlobalNavigation').height());
			});

			$(window).trigger('scroll');

			$('html').addClass('animate');

			$.ajax({
				url: 'local://kHeaderContent.json',
				data: { include: 'menus' },
				dataType: "json",
			}).success(function(e) {
				for (var section in e.menus) {
					var el = $('#KGlobalStaticNavigation li.'+section);
					var content = $('<nav class="imported"></nav>').html(e.menus[section]);

					if (el.has('section').length == 0)
						el.addClass('hasMenu').append('<section></section>');

					$('section', el).prepend(content);
				}
			});
		});
	})( jQuery );
