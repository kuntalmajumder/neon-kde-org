<?php
	define('OVERLAY', true);
	include('templates/header.php');
?>

	<main class="KLayout">
		<section id="swagIntro" class="heroDisplay overlay">
			<article>
				<img src="content/neon-logo.svg" style="width: 100px; height: 100px; margin: 0px auto; display: block;" />
				<h1 style="color: #333">Introducing KDE neon</h1>
				<p>
					The latest and greatest of KDE community software
					packaged on a rock-solid base.
				</p>
				
				<div style="position: relative;">
					<aside id="homeSocialLinks" class="kSocialLinks" style="text-align: left;">News<br />
					        <!--  <a target="_blank" href="https://blog.neon.kde.org"><img src="/images/neon-blog.png" /></a> -->
						<a class="shareNeon" target="_blank" href="https://blog.neon.kde.org/">KDE neon blog</a>
						<a class="shareFacebook" target="_blank" href="https://facebook.com/kdeneon">Share on Facebook</a>
						<a class="shareTwitter" target="_blank" href="https://twitter.com/KdeNeon">Share on Twitter</a></li>
					</aside>
					<aside id="homeSocialLinks2" class="kSocialLinks" style="position: absolute; right: 0px; top: 0px; text-align: left;">Support<br />
						<a class="shareForum" target="_blank" href="https://forum.kde.org/viewforum.php?f=309">KDE Forum</a></li>
						<a class="shareFacebook" target="_blank" href="https://www.facebook.com/groups/931803210238672">KDE neon users Facebook group</a>
						<a class="shareTelegram" target="_blank" href="https://telegram.me/kdeneon">Telegram group</a></li>
						<a class="shareMatrix" target="_blank" href="https://webchat.kde.org/#/room/#kde-neon-users:kde.org">Matrix room</a></li>
					</aside>
				</div>
			</article>
		</section>

		<section>
				<img src="content/home/laptop.png" class="splashImage" />
				<article>
					<h1>Solid Core, Latest Features</h1>
					<p>More than ever people expect a stable desktop with cutting-edge
					features, all in a package which is easy to use and ready to make
					their own.</p>

					<p>KDE neon is the intersection of these needs using a stable Ubuntu
					long-term release as its core, packaging the hottest software
					fresh from the KDE Community ovens. Compute knowing you have a
					solid foundation and enjoy the features you experience in the world's
					most customisable desktop.</p>
					<p>
					You should use KDE neon if you want the latest and greatest from the KDE community but the safety and stability of a Long Term Support release. When you don't want to worry about strange core mechanics and just get things done with the latest features. When you want your computer as your tool, something that belongs to you, that you can trust and that delivers day after day, week after week, year after year. Here it is: now get stuff done.</p>
				</article>


				<article>
					<h1>Make Computing Your Own with Plasma Desktop</h1>
					<p>
						We think that your desktop is YOUR desktop. Make
						it unique with the option to dive into every
						minimal detail from visuals to work patterns.
					</p>
					<p>
						Plasma Desktop from the KDE community is a smart,
						effective, and beautiful environment from the
						very first time you start it. Using KDE neon,
						Plasma and KDE applications will be continuously updated, so no more waiting,
						adding package archives or downloading source code
						if you want what’s new.
					</p>

				</article>
		</section>
	</main>

<?php

	include ('templates/footer.php');
?>
