<?php

require_once('../lib/IsoDates.php');

use PHPUnit\Framework\TestCase;

final class IsoDatesTest extends TestCase
{
    public function testprobeIsoDir() {
        $isoDates = new IsoDates();
        $date = $isoDates->probeIsoDir('user');
        $this->assertStringMatchesFormat('%d-%d', $date);
        $date = $isoDates->probeIsoDir('unstable');
        $this->assertStringMatchesFormat('%d-%d', $date);
        $date = $isoDates->probeIsoDir('testing');
        $this->assertStringMatchesFormat('%d-%d', $date);
        $date = $isoDates->probeIsoDir('developer');
        $this->assertStringMatchesFormat('%d-%d', $date);
    }

    public function testWriteDatesFile() {
        $file = 'data/writeDatesFile.json';
        if (file_exists($file)) {
            unlink($file);
        }
        $isoDates = new IsoDates();
        $isoDates->datesFile = $file;
        $isoDates->init();
        $this->assertFileExists($file);
    }

    public function testReadDatesFile() {
        $file = 'data/readDatesFile.json';
        $isoDates = new IsoDates();
        $isoDates->datesFile = $file;
        $isoDates->init();
        $this->assertEquals('20190616-1105', $isoDates->unstable);
        $this->assertEquals('20190625-1046', $isoDates->testing);
        $this->assertEquals('20190627-1118', $isoDates->user);
        $this->assertEquals('20190625-0000', $isoDates->developer);
    }
}
